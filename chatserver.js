var http, url, fs, server;
http = require('http');
url = require('url');
fs = require('fs');

server = http.createServer(function(req, res) {
	// parse pathname as url
	var path = url.parse(req.url).pathname;

	switch(path) {
		case '/':
		  fs.readFile(__dirname + '/chatclient.html', function(err, data) {
		  	if(err) { return send404(res); }
		  	res.writeHead(200, {'Content-Type': 'text/html'});
		    res.write(data, 'utf8');
		    res.end();	
		  });
		  break;

		 default: send404(res);
	}
});

var send404 = function(res) {
	res.writeHead(404);
	res.write('404: not found');
	res.end();
}

server.listen(8000);
console.log('server started on port 8000');

var io = require('socket.io').listen(server);

// on a connection event
io.sockets.on('connection', function(socket) {
	console.log("connection " + socket.id + " accepted");

	// define event handlers for our connected socket object
	socket.on('disconnect', function() {
	  console.log("connection " + socket.id + " terminated");
    });

    socket.on('message', function(message) {
	  console.log("received message " + message + " - from client " + socket.id);
	  // relay message to all clients
	  io.sockets.emit('chat', socket.id, message);
    });
});