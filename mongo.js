var dbUrl = 'localhost/mongoapp';
var collections = ['users'];
var db = require('mongojs').connect(dbUrl, collections);

// User object model
function User(firstName, lastName, email) {
	this.firstName = firstName;
	this.lastName = lastName;
	this.email = email;
}

// db.users.remove({});

db.users.ensureIndex({email:1}, {unique:true});

function addUser(user) {
	db.users.save(user, function(err, savedUser){
		if(err || !savedUser) {
			console.log("user " + user.email + ' not saved.  [' + err + ']')
		}
		else {
			console.log('saved user: ' + savedUser.email);
    	}
	});	
}

var user1 = new User('Phillip', 'Frost', 'philf@email.com');
var user2 = new User('Johnny', 'Rodgers', 'jrog@email.com');

addUser(user1);
addUser(user2);

db.users.update({firstName: user1.firstName, lastName: user1.lastName}, {$set : {email: 'fry.planex.com'}}, {upsert:false, multi:false}, function(err, updatedUser) {
	if(err) {
		console.log('error updating user: ' + err);
	}
});

db.users.find({}, function(err, users) {
	if (err || !users.length) {
		console.log('No users found.');
	}
	else {
		users.forEach(function(user) {
			console.log("user found: " + JSON.stringify(user));
		});
	}
});

db.users.findOne({email: user1.email}, function(err, foundUser) {
	if(err || !foundUser) {
			console.log("user " + user1.email + ' not found.  [' + err + ']')
		}
		else {
			console.log('found user: ' + foundUser.email);
    	}
});