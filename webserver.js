var http, url, fs, server;
http = require('http');
url = require('url');
fs = require('fs');

server = http.createServer(function(req, res) {
	// parse pathname as url
	var path = url.parse(req.url).pathname;

	switch(path) {
		case '/':
		  fs.readFile(__dirname + '/index.html', function(err, data) {
		  	if(err) { return send404(res); }
		  	res.writeHead(200, {'Content-Type': 'text/plain'});
		    res.write(data, 'utf8');
		    res.end();	
		  });
		  break;

		case '/test':
		  res.writeHead(200, {'Content-Type': 'text/plain'});
		  res.write('It works!\n');
		  res.end();
		  break;

		 default: send404(res);
	}
});

var send404 = function(res) {
	res.writeHead(404);
	res.write('404: not found');
	res.end();
}

server.listen(8000);
console.log('server started on port 8000');
