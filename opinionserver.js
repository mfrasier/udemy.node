'use strict'

//***************
// Express App
//***************
var express = require('express');
var app = express();
var morgan = require('morgan');
var port = 8000;

// app config
//app.set('view engine', 'jade');
//app.set('view options', {layout: false});
//app.set('views', __dirname + '/views');

// middlewares
app.use(morgan('common'));				// logger
// static files
app.use(express.static(__dirname + '/public'));		
app.use(express.static(__dirname + '/node_modules'));		

// routes
app.get('/', function(req, res) {
	res.send('hello\n');
});

app.get('/test', function(req, res) {
	res.send('Hello World!\n');
});

app.get('/vote', function(req, res) {
	res.sendFile(__dirname + '/views/votingclient.html');
});

app.get('/chart', function(req, res) {
	res.sendFile(__dirname + '/views/livechartclient.html');
});

// last middleware - 404
app.use(function(req, res, next) {
	res.status(404).send('Sorry, can\'t find what you were looking for.\n');
});

//*****************
// Socket.io functions
//*****************
var server = require('http').Server(app);
var io = require('socket.io')(server);
var votes = {};

server.listen(port, '0.0.0.0');
console.log('express app started, listening on port ' + port);
//console.log('socket.io started, listening on port ' + port);

// on 'connection' event, all sockets
io.sockets.on('connection', function(socket) {
	console.log("Connection " + socket.id + " accepted.");

	// events for this socket
	// our custom 'vote' event
	socket.on('vote', function(vote) {
		// record vote
		console.log("Client " + socket.id + " voted " + vote);
		votes[socket.id] = vote;	// really?
		print_votes();
	});

	socket.on('ticker', function(fn) {
		console.log("sending vote average to client " + socket.id);
		// calc avg
		var total = 0, ctr = 0;
		for (var v in votes) {
			total += votes[v];
			ctr++;
		}
		// return average to client
		var average = total/ctr;
		console.log("average: " + total + '/' + ctr + '=' + average);
		fn(average);
	});

	socket.on('disconnect', function(vote) {
		console.log("Connection " + socket.id + " terminated");
	});
});

var print_votes = function() {
	var total = 0;
	console.log("\nVotes");
	for (var v in votes) {
		console.log("\tvotes[" + v + "] = " + votes[v]);
	}
	// console.log("\n");
};