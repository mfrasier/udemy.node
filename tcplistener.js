var net = require('net');
var port = 8000;
var client_ctr = 0;

net.createServer(function(socket) {
  client_ctr++;
	console.log("Client " + client_ctr + " connected.");
  socket.write("Connected to server\r\n");
	socket.write("  Commands: disconnect\r\n");
	socket.write("            server address\r\n");
	socket.write("  anything else echoes\r\n");

	socket.on('data', function(data) {
	  console.log('got data: ' + data);
		if (String(data).trim() === "disconnect") {
		  socket.end("thank you for playing, disconnecting.\r\n");
		}
		else if (String(data).trim() === "server address") {
		  var address = socket.address();
		  console.log('address: ' + address.address + ' port:' + address.port)
		  socket.write('address: ' + address.address + ' port:' + address.port + '\r\n')
		}
		else {
	    socket.pipe(socket);
		}
	});

	socket.on('end', function() {
	  console.log('connection ended');
	});
})
.listen(port, "127.0.0.1", function() {
  console.log("tcp listener server started on port " + port);
});


